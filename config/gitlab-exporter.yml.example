db_common: &db_common
  methods:
    - probe_db
  opts: &db_common_opts
    connection_string: dbname=gitlabhq_development user=postgres

# Web server config
server:
  listen_address: 0.0.0.0
  listen_port: 9168
  # Maximum amount of memory to use in megabytes, after which the process is killed
  memory_threshold: 1024

# Probes config
probes:
  # Each key corresponds to an endpoint, so here metrics are available at http://localhost:9168/git.
  # The server will search for a prober using the format `KeyProber`, so here it will be `GitProber`.
  # If there's no prober matching the format above, `class_name` key should be provided (see `git_process` below).
  git:
    # Methods to call on the prober
    methods:
      - probe_pull
      - probe_push
    # Options to pass to the prober class initializer
    opts:
      source: /home/git/repo

  git_process: &git_process
    class_name: GitProcessProber # `class_name` is redundant here
    methods:
    - probe_git
    opts:
      quantiles: true

  database_bloat:
    class_name: Database::BloatProber
    <<: *db_common

  # We can group multiple probes under a single endpoint by setting the `multiple` key to `true`, followed
  # by probe definitions as usual.
  database:
    multiple: true
    ci_builds:
      class_name: Database::CiBuildsProber
      <<: *db_common
      opts:
        <<: *db_common_opts
        allowed_repeated_commands_count: 2
        created_builds_counting_disabled: true
        unarchived_traces_offset_minutes: 1440
    tuple_stats:
      class_name: Database::TuplesProber
      <<: *db_common
    rows_count:
      class_name: Database::RowCountProber
      <<: *db_common
      opts:
        <<: *db_common_opts
        selected_queries:
        - soft_deleted_projects
        - orphaned_projects
        - uploads
    remote_mirrors:
      class_name: Database::RemoteMirrorsProber
      <<: *db_common
      opts:
        <<: *db_common_opts
        project_ids:
        - 1

  process: &process
    methods:
      - probe_stat
      - probe_count
    opts:
      - pid_or_pattern: "sidekiq .* \\[.*?\\]"
        name: sidekiq
      - pid_or_pattern: "unicorn.* worker\\[.*?\\]"
        name: unicorn
      - pid_or_pattern: "git-upload-pack --stateless-rpc"
        name: git_upload_pack
        quantiles: true

  sidekiq: &sidekiq
    methods:
      - probe_stats
      - probe_queues
      - probe_workers
      - probe_retries
    opts:
      redis_url: "redis://localhost:6379"
      redis_enable_client: true

  metrics:
    multiple: true
    git_process:
      <<: *git_process
    process:
      <<: *process
    sidekiq:
      <<: *sidekiq
    ci_builds:
      class_name: Database::CiBuildsProber
      <<: *db_common
    tuple_stats:
      class_name: Database::TuplesProber
      <<: *db_common
    rows_count:
      class_name: Database::RowCountProber
      <<: *db_common

